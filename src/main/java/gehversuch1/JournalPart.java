package gehversuch1;

import javafx.scene.control.TextField;
import javafx.scene.control.Label;

public class JournalPart {
	private Label label;
	private TextField tf;
	
	public JournalPart(Label label, TextField tf) {
		this.label = label;
		this.tf = tf;
	}
	
	public String getName() {
		return label.getText();
	}
	
	public String getContent() {
		return tf.getText();
	}
	
	public void setContent(String text) {
		tf.setText(text);
	}
	
	public Label getLabel() {
		return label;
	}
	
	public TextField getTf() {
		return tf;
	}
}
