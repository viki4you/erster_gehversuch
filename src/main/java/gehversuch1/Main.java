package gehversuch1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.GridPane;

/**
 * Beispielprogramm demonstriert das Action Handling in JavaFX.
 * 
 * @author Michael Inden
 * 
 * Copyright 2014 by Michael Inden 
 */
public class Main extends Application 
{
	@Override
	public void start(final Stage stage) 
	{
		List<String> accountNames = Arrays.asList("SollKontoName",
				"SollKontoNummer", 
				"SollMenge",
				"HabenKontoName",
				"HabenKontoNummer",
				"HabenMenge");
		
		List<JournalPart> journalEntry = new ArrayList<>();
		for(int i = 0; i < 6; i++) {
			journalEntry.add(new JournalPart(new Label(accountNames.get(i)), new TextField()));
		}
		
		final Button btn = new Button();
		btn.setText("Speichern");

		final GridPane pane = new GridPane();
		pane.setPadding(new Insets(7,7,7,7));
		
		for(int i = 0; i < journalEntry.size(); i++) {
			pane.add(journalEntry.get(i).getLabel(), i, 0);
			pane.add(journalEntry.get(i).getTf(), i, 1);
		}
		
		pane.add(btn, 0, 2);

		// ActionHandler registrieren  
		btn.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event) 
			{
				try {
					addEntry(journalEntry.get(0).getTf().getText(),
							journalEntry.get(1).getTf().getText(),
							journalEntry.get(2).getTf().getText(),
							journalEntry.get(3).getTf().getText(),
							journalEntry.get(4).getTf().getText(),
							journalEntry.get(5).getTf().getText());
				} catch (ParserConfigurationException | 
						SAXException | 
						IOException | 
						TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		stage.setScene(new Scene(pane, 800, 600));
		stage.setTitle("JavaFxActionHandlingExample");		
		stage.setResizable(true);
		stage.centerOnScreen();
		stage.show();
	}

	
	public static void main(final String[] args) 
	{
		launch(args);
	}
	
	private void addEntry(String sollName, 
			String sollNummer, 
			String sollMenge,
			String habenName, 
			String habenNummer, 
			String habenMenge) 
					throws ParserConfigurationException, 
					SAXException, 
					IOException, 
					TransformerException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("Data.xml");
        Element root = document.getDocumentElement();
        System.out.println(root.getClass());
        String id;
        try {
        	id = root.getLastChild().getNodeName();//getAttributes().getNamedItem("id").toString();//.getNodeValue().toString();
        }catch(NullPointerException npe) {
        	id = "0";
        }
        Integer i = new Integer(id);
        i++;
        Element newEntry = document.createElement("entry");
        newEntry.setAttribute("id", i.toString());

        Element soll = document.createElement("soll");
        
        Element accountname = document.createElement("accountname");
        accountname.appendChild(document.createTextNode(sollName));
        soll.appendChild(accountname);
        
        Element accountnummer = document.createElement("accountnummer");
        accountnummer.appendChild(document.createTextNode(sollNummer));
        soll.appendChild(accountnummer);
        
        Element amount = document.createElement("amount");
        amount.appendChild(document.createTextNode(sollMenge));
        soll.appendChild(amount);
        
        newEntry.appendChild(soll);
        
        Element haben = document.createElement("haben");
        
        accountname = document.createElement("accountname");
        accountname.appendChild(document.createTextNode(habenName));
        haben.appendChild(accountname);
        
        accountnummer = document.createElement("accountnummer");
        accountnummer.appendChild(document.createTextNode(habenNummer));
        haben.appendChild(accountnummer);
        
        amount = document.createElement("amount");
        amount.appendChild(document.createTextNode(habenMenge));
        haben.appendChild(amount);
        
        newEntry.appendChild(haben);
        
        root.appendChild(newEntry);
        
        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult("Data.xml");
        transformer.transform(source, result);
	}
}